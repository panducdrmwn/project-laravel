<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view('form');
    }

    public function kirim(Request $request){
        $nama = $request['nama'];

        return view('selamat', compact('nama'));
    }
}
